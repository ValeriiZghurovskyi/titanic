import pandas as pd 
 
def get_titatic_dataframe() -> pd.DataFrame: 
    df = pd.read_csv("train.csv") 
    return df 
 
def get_filled(): 
    df = get_titatic_dataframe() 
 
    mr_group = df[df['Name'].str.contains('Mr\.')] 
    mrs_group = df[df['Name'].str.contains('Mrs\.')] 
    miss_group = df[df['Name'].str.contains('Miss\.')] 
     
    mr_median_age = round(mr_group['Age'].median()) 
    mrs_median_age = round(mrs_group['Age'].median()) 
    miss_median_age = round(miss_group['Age'].median()) 
     
    df.loc[df['Name'].str.contains('Mr\.'), 'Age'] = mr_median_age 
    df.loc[df['Name'].str.contains('Mrs\.'), 'Age'] = mrs_median_age 
    df.loc[df['Name'].str.contains('Miss\.'), 'Age'] = miss_median_age 
     
    mr_missing_count = mr_group['Age'].isna().sum() 
    mrs_missing_count = mrs_group['Age'].isna().sum() 
    miss_missing_count = miss_group['Age'].isna().sum() 
     
    result = [('Mr.', mr_missing_count, mr_median_age), 
              ('Mrs.', mrs_missing_count, mrs_median_age), 
              ('Miss.', miss_missing_count, miss_median_age)] 
     
    return result